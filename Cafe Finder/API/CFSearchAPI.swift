//
//  CFSearchAPI.swift
//  Cafe Finder
//
//  Created by Mark Nigos on 08/01/2018.
//  Copyright © 2018 Mark Nigos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class CFSearchAPI: NSObject {
    private static let apiKey = "AIzaSyAxLl3yLQtk7YxUtl5Q_y_CT8yydgb87po"
    private static let locationType = "cafe"
    
    static func handleAPI(
        url: String!,
        parameters: [String: Any]? = nil,
        onComplete: @escaping (_ data: CFApiResponseModel) -> ()) {
        
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters
            ).responseJSON { (responseData) in
                onComplete(CFApiResponseModel(data: JSON(responseData.result.value!)))
        }
    }
    
    class func getCafeNearUser(
        coordinates: CLLocationCoordinate2D,
        onSuccess: @escaping (_ data: [CFCafeModel]) -> (),
        onFailed: @escaping (_ error: String, _ code: Int) -> ()) {
        let parameters = [
            "location": "\(String(coordinates.latitude)),\(String(coordinates.longitude))",
            "rankby": "distance",
            "type": locationType,
            "key": apiKey
        ]
        
        let userLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        handleAPI(url: "https://maps.googleapis.com/maps/api/place/nearbysearch/json",
                  parameters: parameters) { (responseData) in
                    if responseData.status != "OK" || (responseData.results.array?.isEmpty)! {
                        onFailed("An Error has occured. Please Contact Support.", 0)
                    } else {
                        var cafes:[CFCafeModel] = []
                        for cafeData in responseData.results.array! {
                            let cafe = CFCafeModel(data: cafeData)
                            let cafeLocation = CLLocation(latitude: Double(cafe.latitude), longitude: Double(cafe.longitude))
                            cafe.distance = getDistance(userLocation: userLocation, storeLocation: cafeLocation)
                            cafes.append(cafe)
                        }
                        onSuccess(cafes)
                    }
        }
    }
    
    class func searchCafe(
        searchText: String,
        coordinates: CLLocationCoordinate2D,
        onSuccess: @escaping (_ data: [CFCafeModel]) -> (),
        onFailed: @escaping (_ error: String, _ code: Int) -> ()) {
        let parameters = [
            "location": "\(String(coordinates.latitude)),\(String(coordinates.longitude))",
            "query": searchText,
            "rankby": "distance",
            "type": locationType,
            "key": apiKey
        ]
        
        let userLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        handleAPI(url: "https://maps.googleapis.com/maps/api/place/textsearch/json",
                  parameters: parameters) { (responseData) in
                    if responseData.errorMessage != nil {
                        onFailed("An Error has occured. Please Contact Support.", 0)
                    } else {
                        var cafes:[CFCafeModel] = []
                        for cafeData in responseData.results.array! {
                            let cafe = CFCafeModel(data: cafeData)
                            let cafeLocation = CLLocation(latitude: Double(cafe.latitude), longitude: Double(cafe.longitude))
                            cafe.distance = getDistance(userLocation: userLocation, storeLocation: cafeLocation)
                            cafes.append(cafe)
                        }
                        onSuccess(cafes)
                    }
        }
        
    }
    
    private static func getDistance(userLocation: CLLocation, storeLocation: CLLocation) -> String {
        let distance = storeLocation.distance(from: userLocation)
        let kmDistance = distance/1000.0
        return String(format: "%.2f km away", kmDistance)
    }
}
