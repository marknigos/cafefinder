//
//  CFDashboardViewController.swift
//  Cafe Finder
//
//  Created by Mark Nigos on 08/01/2018.
//  Copyright © 2018 Mark Nigos. All rights reserved.
//

import UIKit
import CoreLocation

class CFDashboardViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var cvCafes: UICollectionView!

    //MARK: - Variables
    var locationManager: CLLocationManager!
    var currCoordinates: CLLocationCoordinate2D!
    var cafeList: [CFCafeModel] = []
    
    var searchText = ""
    
    //MARK: - VC Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserLocation()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue" {
            let detailsVC = segue.destination as! CFCafeDetailsViewController
            let cafe = cafeList[(cvCafes.indexPathsForSelectedItems?[0].row)!]
            detailsVC.cafe = cafe
            detailsVC.userLocation = currCoordinates
        }
    }
    //MARK: - Custom Methods
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
//            locationManager.startUpdatingHeading()
        }
    }
    
    func getNearestCafes() {
        CFSearchAPI.getCafeNearUser(
            coordinates: currCoordinates,
            onSuccess: { (cafes) in
                self.cafeList = cafes
                self.cvCafes.reloadData()
        },
            onFailed: { (error, code) in
                print(error)
        })
    }
    
    func searchCafe() {
        CFSearchAPI.searchCafe(
            searchText: searchText,
            coordinates: currCoordinates,
            onSuccess: { (cafes) in
                self.cafeList = cafes
                self.cvCafes.reloadData()

        },
            onFailed: { (error, code) in
                print(error)
        })
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
}

//MARK: - CLLocationManagerDelegate
extension CFDashboardViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currCoordinates = manager.location?.coordinate
        print("locations = \(currCoordinates.latitude) \(currCoordinates.longitude)")
        getNearestCafes()
        locationManager.stopUpdatingLocation()
    }
}

//MARK: - CollectionView DataSource
extension CFDashboardViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cafeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CFCafeCellID", for: indexPath) as! CFDashboardItemCollectionViewCell
        
        let cafe = cafeList[indexPath.row]
        
        if cafe.icon != nil {
            cell.imgIcon.image = cafe.icon
        } else {
            let url = URL(string: cafe.iconUrl)!
            self.getDataFromUrl(url: url) { data, response, error in
                guard let data = data, error == nil else { return }
//                print(response?.suggestedFilename ?? url.lastPathComponent)
//                print("Download Finished")
                DispatchQueue.main.async() {
                    cafe.icon = UIImage(data: data)
                    cell.imgIcon.image = cafe.icon
                }
            }
        }
        
        if cafe.openingHours.openNow {
            cell.lblStatus.text = "OPEN"
            cell.lblStatus.backgroundColor = UIColor(red: 0/255.0, green: 94/255.0, blue: 2.0/255.0, alpha: 1.0)
        } else {
            cell.lblStatus.text = "CLOSE"
            cell.lblStatus.backgroundColor = UIColor(red: 89.0/255.0, green: 2.0/255.0, blue: 2.0/255.0, alpha: 1.0)
        }

        cell.imgIcon.image = cafe.icon
        cell.lblName.text = cafe.name
        cell.lblDistance.text = cafe.distance
        cell.lblAddress.text = cafe.vicinity
        
        return cell
    }
}

//MARK: - CollectionView Delegate
extension CFDashboardViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
}

extension CFDashboardViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        searchBar.enablesReturnKeyAutomatically = (searchText == "")
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.showsCancelButton = false
        searchText = searchBar.text!
        
        if searchText == "" {
            getNearestCafes()
        } else {
            searchCafe()
        }
        print("search")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.text = searchText
        searchBar.showsCancelButton = false

        print("cancel")
    }
}
