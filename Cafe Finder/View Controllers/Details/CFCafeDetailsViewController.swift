//
//  CFCafeDetailsViewController.swift
//  Cafe Finder
//
//  Created by Mark Nigos on 09/01/2018.
//  Copyright © 2018 Mark Nigos. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class CFCafeDetailsViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK: - Variables
    var cafe: CFCafeModel!
    var userLocation: CLLocationCoordinate2D!
    
    //MARK: - VC Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = cafe.name
        imgIcon.image = cafe.icon
        lblAddress.text = cafe.vicinity
        lblDistance.text = cafe.distance
        
        initiateMap()
    }
    
    //MARK: - Custom Methods
    func initiateMap(){
        let cafeLocation = CLLocationCoordinate2D(latitude: cafe.latitude, longitude: cafe.longitude)
        
        let userPlacemark = MKPlacemark(coordinate: userLocation, addressDictionary: nil)
        let cafePlacemark = MKPlacemark(coordinate: cafeLocation, addressDictionary: nil)
        
        let userMapItem = MKMapItem(placemark: userPlacemark)
        let cafeMapItem = MKMapItem(placemark: cafePlacemark)
        
        let userAnnotation = MKPointAnnotation()
        userAnnotation.title = "You"

        if let location = userPlacemark.location {
            userAnnotation.coordinate = location.coordinate
        }
        
        let cafeAnnotation = MKPointAnnotation()
        cafeAnnotation.title = cafe.name
        
        if let location = cafePlacemark.location {
            cafeAnnotation.coordinate = location.coordinate
        }

        self.mapView.showAnnotations([userAnnotation,cafeAnnotation], animated: true )

        let directionRequest = MKDirectionsRequest()
        directionRequest.source = userMapItem
        directionRequest.destination = cafeMapItem
        directionRequest.transportType = .walking

        let directions = MKDirections(request: directionRequest)

        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    }
    
}

//MARK: - MKMapViewDelegate
extension CFCafeDetailsViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        
        return renderer
    }
}
