//
//  CFCafeModel.swift
//  Cafe Finder
//
//  Created by Mark Nigos on 09/01/2018.
//  Copyright © 2018 Mark Nigos. All rights reserved.
//

import UIKit
import SwiftyJSON

class CFCafeModel: NSObject {
    var storeId: String!
    var iconUrl: String!
    var name: String!
    var reference: String!
    var placeId: String!
    var priceLevel: Double!
    var rating: Double!
//    var photos: JSON!
    var scope: String!
    var vicinity: String!
//    var geometry: JSON!
    var latitude: Double!
    var longitude: Double!
    var openingHours: CFCFCafeHoursModel!
    
    //not included in init
    var distance: String!
    var icon: UIImage!

    init(data: JSON) {
        storeId      = data["id"].string
        iconUrl      = data["icon"].string
        name         = data["name"].string
        reference    = data["reference"].string
        placeId      = data["place_id"].string
        priceLevel   = data["price_level"].double
        rating       = data["rating"].double
//        photos       = data["photos"]
        scope        = data["scope"].string
        vicinity     = data["vicinity"].string
        if vicinity == nil {
            vicinity = data["formatted_address"].string
        }
//        geometry     = data["geometry"]
        latitude     = data["geometry"]["location"]["lat"].double
        longitude    = data["geometry"]["location"]["lng"].double
        openingHours = CFCFCafeHoursModel(data: data["opening_hours"])
    }
}

//class CFCFCafePhotosModel: NSObject {
//}

//class CFCFCafeGeometryModel: NSObject {
//}

class CFCFCafeHoursModel: NSObject {
    var weekdayText: Array<Any>!
    var openNow: Bool!
    init(data: JSON) {
        weekdayText = data["weekday_text"].array
        openNow     = data["open_now"].boolValue
    }
}


