//
//  CFApiResponseModel.swift
//  Cafe Finder
//
//  Created by Mark Nigos on 09/01/2018.
//  Copyright © 2018 Mark Nigos. All rights reserved.
//

import UIKit
import SwiftyJSON

class CFApiResponseModel: NSObject {
    var status: String!
    var results: JSON!
    var htmlAttributions: JSON!
    var nextPageToken: String!
    var errorMessage: String!
    
    init(data : JSON) {
        status           = data["status"].string
        results          = data["results"]
        htmlAttributions = data["html_attributions"]
        nextPageToken    = data["next_page_token"].string
        errorMessage     = data["error_message"].string
    }

}
