//
//  CFDashboardItemCollectionViewCell.swift
//  Cafe Finder
//
//  Created by Mark Nigos on 09/01/2018.
//  Copyright © 2018 Mark Nigos. All rights reserved.
//

import UIKit

class CFDashboardItemCollectionViewCell: UICollectionViewCell {
    //MARK: - Outlets
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
}
